<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Index</title>

  <!-- Riferimenti a bootstrap 4 prese da w3school -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

  <!-- Stili -->
  <style>
        body, html {   
          width: 100%;
          height: 100%;
          margin: 0;
          padding: 0;
          display:table;
        }
        body {
            display:table-cell;
            vertical-align:middle;
            text-align: center;
        }
        form {
            display:table;/* shrinks to fit content */
            margin:auto;
        }
        .imgcontainer {
            text-align: center;
            margin: px 0 12px 0;
            }
        img.avatar {
            width: 50%;
            border-radius: 50%;
        }
        button {
   position: fixed;
   left: 50%;
   top:50%;
 }
  </style>

  <!-- Funzione MostraPassword -->
  <script>
      function MostraPassword() {
        var x = document.getElementById("myInput");
          if (x.type === "password") {
            x.type = "text";
          } else {
            x.type = "password";
          }
      }
  </script>
</head>
<body>

  <!-- Immagine Autech -->
  <form method="POST" action="contratti.php">
    <div class="text-center">
      <img src="images/autech.png" alt="Avatar" class="avatar">

      <!-- Messaggio errore Credenziali errate -->
      <?php
        if (isset($_GET["error"]) && $_GET["error"]){
          echo "<div class='alert alert-danger'>
                <strong>Errore! </strong>Credenziali errate! 
                </div>";
        }
      ?> 

      <!-- Username -->  
      <div class="input-group mb-3 input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">Username</span>
        </div>
          <input type="text" placeholder="Enter Username" class="form-control" name="user" required>
      </div>
      
      <!-- Password  -->   
      <div class="input-group mb-3 input-group-sm">
        <div class="input-group-prepend">
          <span class="input-group-text">Password</span>  
        </div>       
          <input type="password" id="myInput" placeholder="Enter Password" class="form-control" name="pass" required>
            <div class="input-group-prepend">

              <!-- MostraPassword  --> 
              <span class="input-group-text">Show</span>  
                <div class="input-group-text">
                  <input type="checkbox" onclick="MostraPassword()">
                </div>
            </div>
        </div>

        <!-- Login  -->      
      <center>   
      <input type="submit" class="form-control" value="Login" style="width: 25%;">
      </center>
        <!-- RememberMe e ForgotPassword  -->  
        
        <!--
        <label>
          <input type="checkbox" style="text-align: center" checked="checked" name="remember"> Remember me
        </label>
        <div class="container">
          <span class="psw">Forgot <a href="#">password?</a></span>
        </div>
        -->
    </div>
  </form>
</body>
</html>