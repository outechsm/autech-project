<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <title>Interventi</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <style>
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 100%;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            background-position: 10px 10px; 
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
        }
        th, td {
            text-align: center;
            padding: 16px;
        }
        tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        input[type=text] {
            width:100%;
            box-sizing: border-box;
            border: 2px solid #ccc;
            border-radius: 4px;
            font-size: 16px;
            background-color: white;
            background-image: url('http://localhost/autech-project/images/search_icon.png');
            background-size: 2%;
            background-position: 10px 10px;
            background-repeat: no-repeat;
            padding: 12px 20px 12px 40px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }
    </style>

    <script>
        function link(id, codcli){
            if (id != ""){
                window.location.href = 'http://localhost/autech-project/interventi.php/?id=' + id + "&codcli=" + codcli;
            }
            else{
                window.location.href = 'http://localhost/autech-project/contratti.php/?codcli=' + codcli;
            }
        }

        function SeachBarOurs() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 1; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }          
            }
        }
    </script>
</head>
<body>
    <?php
        function validita($dataFin, $nContatti){
            $datetime1 = new DateTime('now');
            $datetime2 = new DateTime($dataFin);
            $difference = $datetime1->diff($datetime2);
            $risul = (int)$difference->format("%R%a");
            if (($risul < 0) || ($nContatti == 20)){
                return "Expired";
            }
            return "Active";
        }

        $user = ""; $pass = ""; $vuoto = false;

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "autech";
        $dsn = "mysql:host=$servername;dbname=$dbname;";
        $query1 = "";

        if (isset($_GET['id']) && isset($_GET['codcli'])){
            $query1 = "SELECT contratti_help_desk.CodiceInterno AS 'HelpDesk', articoli.Descrizione, contratti_help_desk.DataInizio, 
                        contratti_help_desk.DataFine, contratti_help_desk.NrContatti, clienti.CodiceInterno, clienti.Nome,
                        clienti.IndirizzoDiFatturazione, contratti_help_desk.ID
                        FROM contratti_help_desk
                        INNER JOIN clienti ON contratti_help_desk.IdCliente = clienti.ID
                        INNER JOIN articoli ON contratti_help_desk.IdArticolo = articoli.ID
                        WHERE clienti.CodiceInterno = '{$_GET['codcli']}' AND contratti_help_desk.ID = '{$_GET['id']}'";
        }
        else if (isset($_GET['codcli'])){
            $query1 = "SELECT contratti_help_desk.CodiceInterno AS 'HelpDesk', articoli.Descrizione, contratti_help_desk.DataInizio, 
                        contratti_help_desk.DataFine, contratti_help_desk.NrContatti, clienti.CodiceInterno, clienti.Nome,
                        clienti.IndirizzoDiFatturazione, contratti_help_desk.ID
                        FROM contratti_help_desk
                        INNER JOIN clienti ON contratti_help_desk.IdCliente = clienti.ID
                        INNER JOIN articoli ON contratti_help_desk.IdArticolo = articoli.ID
                        WHERE clienti.CodiceInterno = '{$_GET['codcli']}'";
        }
        else{
            $user = $_POST['user']; $pass = $_POST['pass'];
            $query1 = "SELECT contratti_help_desk.CodiceInterno AS 'HelpDesk', articoli.Descrizione, contratti_help_desk.DataInizio, 
                        contratti_help_desk.DataFine, contratti_help_desk.NrContatti, clienti.CodiceInterno, clienti.Nome,
                        clienti.IndirizzoDiFatturazione, contratti_help_desk.ID
                        FROM contratti_help_desk
                        INNER JOIN clienti ON contratti_help_desk.IdCliente = clienti.ID
                        INNER JOIN articoli ON contratti_help_desk.IdArticolo = articoli.ID
                        WHERE clienti.LoginUsername = :user AND clienti.LoginPassword = :pass";
        }

        try {
            $conn = new PDO($dsn, $username, $password);
            // creazione ed esecuzione della query 1
            $stmt = $conn->prepare($query1);
            $stmt->bindParam(":user",$user);
            $stmt->bindParam(":pass",$pass);
            $stmt->execute();
            // il risultato viene trasformato in array associativo
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($result) == 0){
                $vuoto = true;
                echo $_GET['codcli'];
            }
        }
        catch(PDOException $e)
        {
            echo "Error: " . $e->getMessage();
        }
        echo "<form id='form1'>";
    ?>
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-sm-4' style='background-color:#;'>
                <center>
                <?php echo "
                            <p>
                                User: {$result[0]["CodiceInterno"]}<br>
                                {$result[0]["Nome"]}<br>
                                {$result[0]["IndirizzoDiFatturazione"]}
                            </p>";
                        if (isset($_GET['id']) && isset($_GET['codcli'])){
                            echo "<input type='button' class='btn btn-light' value='Remove filters' onclick=\"link('','{$result[0]['CodiceInterno']}')\">";
                        }
                ?>
                </center>
            </div>

            <div class='col-sm-8' style='background-color:#;'>
            <input type="text" id="myInput" onkeyup="SeachBarOurs()" placeholder="Search for HelpDesk...">
                    <?php
                    if (!$vuoto){
                        echo "<div class='table-responsive'>
                                <table id='myTable'>
                                    <tdead>
                                        <tr>
                                            <td>N°</td>
                                            <td>HelpDesk</td>
                                            <td>Type</td>
                                            <td>From</td>
                                            <td>To</td>
                                            <td>N° Contracts</td>
                                            <td>State</td>
                                        </tr>
                                    </tdead>
                                    <tbody>";
                                        for ($i = 0; $i < count($result); $i++){
                                            echo "
                                                <tr>
                                                <td><input type='button' class='btn btn-light' value='" . ($i + 1) . "' onclick=\"link('{$result[$i]['ID']}', '{$result[0]['CodiceInterno']}')\"/></td>
                                                <td>{$result[$i]['HelpDesk']}</td>
                                                <td>{$result[$i]['Descrizione']}</td>
                                                <td>{$result[$i]['DataInizio']}</td>
                                                <td>{$result[$i]['DataFine']}</td>
                                                <td>{$result[$i]['NrContatti']}</td>
                                                <td>" . validita($result[$i]['DataFine'], $result[$i]['NrContatti']) . "</td></tr>
                                            ";
                                        }
                        echo "</tbody></table></div>";
                    }
                    else{
                        header("Location: http://localhost/autech-project/?error=true");
                        exit;
                    }
                    $conn = false;
                ?>
            </div>
        </div>
    </div>
    </form>
</body>
</html>